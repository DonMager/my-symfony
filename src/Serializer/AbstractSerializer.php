<?php


namespace App\Serializer;

use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

abstract class AbstractSerializer
{
    private ObjectNormalizer $normalizer;

    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    /**
     * @param object|array|null $object
     * @throws ExceptionInterface
     * @throws \Exception
     */
    public function normalize($object, string $format = null, array $context = []): array
    {
        $serializer = $this->getSerializer();
        $data = $serializer->normalize($object, $format, $context);
        if (is_array($data)) {
            return $data;
        }
        throw new \Exception('Не является массивом');
    }

    protected function getSerializer(array $encoders=[]): Serializer
    {
        return new Serializer([$this->normalizer], $encoders);
    }

    /**
     * @return object
     */
    abstract public function denormalize(object $data);
}
