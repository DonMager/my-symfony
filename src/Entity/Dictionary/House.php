<?php

namespace App\Entity\Dictionary;

use App\Entity\Realty;
use App\Repository\Dictionary\HouseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HouseRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class House
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="float", length=5)
     */
    private float $itemNumber;

    /**
     * @ORM\Column(type="string")
     */
    private string $address;

    /**
     * @ORM\Column(type="integer", length=5)
     */
    private string $floatCount;

    /**
     * @ORM\Column(type="float", length=11)
     */
    private float $squareTotal;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Realty", mappedBy="house")
     */
    private $realtyHouse;

    public function __construct()
    {
        $this->realtyHouse = new ArrayCollection();
    }

    /**
     * @return Collection|Realty[]
     */
    public function getRealtyHouse(): Collection
    {
        return $this->realtyHouse;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getItemNumber(): float
    {
        return $this->itemNumber;
    }

    /**
     * @param float $itemNumber
     */
    public function setItemNumber(float $itemNumber): void
    {
        $this->itemNumber = $itemNumber;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getFloatCount(): string
    {
        return $this->floatCount;
    }

    /**
     * @param string $floatCount
     */
    public function setFloatCount(string $floatCount): void
    {
        $this->floatCount = $floatCount;
    }

    /**
     * @return float
     */
    public function getSquareTotal(): float
    {
        return $this->squareTotal;
    }

    /**
     * @param float $squareTotal
     */
    public function setSquareTotal(float $squareTotal): void
    {
        $this->squareTotal = $squareTotal;
    }
}
