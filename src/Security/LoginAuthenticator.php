<?php

namespace App\Security;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

final class LoginAuthenticator
{
    private EntityManagerInterface $entityManager;
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @throws \Exception
     */
    private function getCredentials(Request $request): array
    {
        $body = $request->getContent();
        if (!is_string($body)) {
            throw new \Exception('Нет JSON-объекта');
        }
        $data = json_decode($body, true);

        if (empty($data)) {
            throw new \Exception('Пустой массив данных');
        }

        $credentials = [
            'email' => key_exists('email', $data) ? $data['email'] : "",
            'password' => key_exists('password', $data) ? $data['password'] : "",
        ];

        return $credentials;
    }

    /**
     * @throws \Exception
     */
    public function getApiToken(Request $request): object
    {
        $credentials =  $this->getCredentials($request);
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $credentials['email']]);

        if (!$user) {
            throw new \Exception('Такой учетной записи нет');
        }

        if (!$this->checkCredentials($credentials, $user)) {
            throw new \Exception('Неверный логин или пароль');
        }

        return $user;
    }

    private function checkCredentials(array $credentials, UserInterface $user): bool
    {
        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }
}
