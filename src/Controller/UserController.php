<?php

namespace App\Controller;

use App\Handler\User\RegisterHandler;
use App\Handler\User\UpdateHandler;
use App\Repository\UserRepository;
use App\Serializer\UserSerializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * Class UserController.
 *
 * @Route("/api/user", name="api_user")
 */
class UserController extends AbstractController
{
    private const CODE_SUCCESS = 200;

    private UserRepository $repository;
    private UserSerializer $userSerializer;
    private RegisterHandler $registerHandler;
    private UpdateHandler $updateHandler;

    public function __construct(
        UserRepository $repository,
        UserSerializer $userSerializer,
        RegisterHandler $registerHandler,
        UpdateHandler $updateHandler
    ) {
        $this->repository = $repository;
        $this->userSerializer = $userSerializer;
        $this->registerHandler = $registerHandler;
        $this->updateHandler = $updateHandler;
    }

    /**
     * @Route("/all", name="api_user_all", methods={"GET"})
     * @throws ExceptionInterface
     */
    public function getUsers(): JsonResponse
    {
        $query = $this->repository->findAll();
        return $this->response($this->userSerializer->normalize($query, null, $this->userSerializer->getContextIgnore()));
    }

    /**
     * @Route("/add", name="api_user_add", methods={"POST"})
     * @throws ExceptionInterface
     */
    public function add(Request $request): JsonResponse
    {
        try {
            $data = $this->getJsonDecodeData($request);
            $user = $this->userSerializer->denormalize($data);
            $api = $this->registerHandler->handle($user);
            $data= ['apiToken'=> $api];
            return $this->response($data);
        } catch (\Exception $e) {
            $data =['error'=> $e->getMessage()];
            return $this->response($data, 450);
        }
    }

    /**
     * @Route("/update", name="api_user_update", methods={"POST"})
     * @throws ExceptionInterface
     */
    public function update(Request $request): JsonResponse
    {
        try {
            $data = $this->getJsonDecodeData($request);
            $apiToken = $data->{'apiToken'};
            $user = $this->repository->findByApiToken($apiToken);
            if (!$user) {
                throw new \Exception("Такой токен не существует");
            }
            $user = $this->userSerializer->denormalize($data, $user);
            $this->updateHandler->handle($user);
            $data= ['message'=> "Успешно сохранен"];
            return $this->response($data);
        } catch (\Exception $e) {
            $data =['error'=> $e->getMessage()];
            return $this->response($data, 450);
        }
    }

    private function response(array $data, int $status = self::CODE_SUCCESS, array $headers = []): JsonResponse
    {
        return new JsonResponse($data, $status, $headers);
    }

    /**
     * @throws \Exception
     */

    private function getJsonDecodeData(Request $request): object
    {
        $body = $request->getContent();
        if (!is_string($body)) {
            throw new \Exception('Нет JSON-объекта');
        }
        $data = json_decode($body);
        if ($data) {
            return $data;
        }
        throw new \Exception('Нет данных');
    }
}
