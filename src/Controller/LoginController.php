<?php

namespace App\Controller;

use App\Handler\User\RegisterHandler;
use App\Repository\UserRepository;
use App\Security\LoginAuthenticator;
use App\Serializer\UserSerializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class LoginController extends AbstractController
{
    private const CODE_SUCCESS = 200;
    private LoginAuthenticator $loginAuthenticator;
    private UserSerializer $userSerializer;

    public function __construct(
        LoginAuthenticator $loginAuthenticator,
        UserSerializer $userSerializer
    ) {
        $this->loginAuthenticator = $loginAuthenticator;
        $this->userSerializer  = $userSerializer;
    }

    /**
     * @Route("/api/login", name="api_login", methods={"POST"})
     * @throws ExceptionInterface
     */
    public function getLogin(Request $request): JsonResponse
    {
        try {
            $user = $this->loginAuthenticator->getApiToken($request);
            return $this->response($this->userSerializer->normalize($user, null, $this->userSerializer->getContextIgnore()));
        } catch (\Exception $e) {
            $data =['error'=> $e->getMessage()];
            return $this->response($data, 450);
        }
    }

    private function response(array $data, int $status = self::CODE_SUCCESS, array $headers = []): JsonResponse
    {
        return new JsonResponse($data, $status, $headers);
    }
}
