<?php


namespace App\Handler\Dictionary\House;

use App\Entity\Dictionary\House;
use App\Repository\Dictionary\HouseRepository;

class AddHandler
{
    private HouseRepository $repository;

    public function __construct(
        HouseRepository $repository
    ) {
        $this->repository = $repository;
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public function handle(House $house): void
    {
        $repository = $this->repository;
        $repository->save($house);
    }
}
