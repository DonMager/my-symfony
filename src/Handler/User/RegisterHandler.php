<?php


namespace App\Handler\User;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\PasswordEncoder;
use App\Security\TokenGenerator\TokenGenerator;

class RegisterHandler
{
    private UserRepository $userRepository;
    private TokenGenerator $tokenGenerator;
    private PasswordEncoder $passwordEncoder;

    public function __construct(
        UserRepository $userRepository,
        TokenGenerator $tokenGenerator,
        PasswordEncoder $passwordEncoder
    ) {
        $this->userRepository = $userRepository;
        $this->tokenGenerator = $tokenGenerator;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */

    public function handle(User $user): string
    {
        $repository = $this->userRepository;
        if ($repository->findByEmail($user->getEmail())) {
            throw new \Exception(" Такая электронная почта есть в  системе");
        }
        if ($repository->findByPhone($user->getPhone())) {
            throw new \Exception(" Такой номер телефона есть в  системе");
        }
        $user->setRoles(['ROLE_USER']);
        $user->setVerifyToken($this->tokenGenerator->generate());
        $user->setApiToken($this->tokenGenerator->generate());
        $this->passwordEncoder->encode($user);
        $repository->save($user);
        return  $user->getApiToken();
    }
}
