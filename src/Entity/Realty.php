<?php

namespace App\Entity;

use App\Entity\Dictionary\House;
use Symfony\Component\Serializer\Annotation\Ignore;
use App\Repository\RealtyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RealtyRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Realty
{
    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="realty")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private User $user;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Dictionary\House", inversedBy="realtyHouse")
     * @ORM\JoinColumn(name="house_id", referencedColumnName="id")
     */
    private House $house;

    /**
     * @ORM\Column(type="float", length=5)
     */
    private float $itemNumber;

    /**
     * @ORM\Column(type="float", length=11)
     */
    private float $square;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('living', 'commercial', 'non-living')")
     */
    private string $itemType = 'living';

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return House
     */
    public function getHouse(): House
    {
        return $this->house;
    }

    /**
     * @param House $house
     */
    public function setHouse(House $house): void
    {
        $this->house = $house;
    }

    /**
     * @return float
     */
    public function getItemNumber(): float
    {
        return $this->itemNumber;
    }

    /**
     * @param float $itemNumber
     */
    public function setItemNumber(float $itemNumber): void
    {
        $this->itemNumber = $itemNumber;
    }

    /**
     * @return float
     */
    public function getSquare(): float
    {
        return $this->square;
    }

    /**
     * @param float $square
     */
    public function setSquare(float $square): void
    {
        $this->square = $square;
    }

    /**
     * @return string
     */
    public function getItemType(): string
    {
        return $this->itemType;
    }

    /**
     * @param string $itemType
     */
    public function setItemType(string $itemType): void
    {
        $this->itemType = $itemType;
    }
}
