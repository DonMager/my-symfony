<?php


namespace App\Handler\User;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\PasswordEncoder;
use App\Security\TokenGenerator\TokenGenerator;

class UpdateHandler
{
    private UserRepository $userRepository;

    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public function handle(User $user): void
    {
        $this->userRepository->update();
    }
}
