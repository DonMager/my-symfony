<?php

namespace App\Serializer\Dictionary;

use App\Entity\Dictionary\House;
use App\Serializer\AbstractSerializer;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class HouseSerializer extends AbstractSerializer
{
    /**
     * @throws ExceptionInterface
     * @throws \Exception
     */
    public function denormalize(object $data, House $house = null): House
    {
        $context = $house ? [AbstractNormalizer::OBJECT_TO_POPULATE => $house] : [];
        $serializer = $this->getSerializer();
        $house = $serializer->denormalize($data, House::class, 'json', $context);
        if ($house instanceof House) {
            return $house;
        }
        throw new \Exception('Не является объектом House');
    }
}
