<?php


namespace App\Firebase;

use Kreait\Firebase\Messaging;
use Kreait\Firebase\Messaging\CloudMessage;

class Message
{
    private $messaging;

    public function __construct(Messaging $messaging)
    {
        $this->messaging = $messaging;
    }

    /**
     * @throws \Kreait\Firebase\Exception\FirebaseException
     * @throws \Kreait\Firebase\Exception\MessagingException
     */

    public function send()
    {
        $deviceTokens = ["cnsLnniHQA-4P5eIODR8HP:APA91bFgA_TkTiyJQoqhYfVINC-nsAVq7k4wn39EBe3UBNeE771lWEn6cYpzcJQLc5dp8lMJSrSb43TXjIKOAA37dt-qRzGMDCOuePvSGFChZQ9ub1wI_fD6kNeBSiaRAFgpN6Fr2jJQ",
        "d27ptNEdQLSxGnxBuO4hyg:APA91bF-cYbFCfKPPmAht8e3G6LgRfwgyP6vcpmqChceGRXIdCSL8oGQ8Uex1KOQSPY0iX2bP1Qr7-3ivyPGn_NedZ2GnbizfdxVYoycAJW_36NTKmm0xOTodoIUpPp1tBHaHs7Ff05F"];
        $topic = "a-topic";
        $this->messaging->subscribeToTopic($topic, $deviceTokens);
        $message = CloudMessage::withTarget('topic', $topic)
                ->withNotification(['title' => 'Симфони 2', 'body' => 'Привет 4']);
        $this->messaging->send($message);
    }

    /**
     * @throws \Kreait\Firebase\Exception\FirebaseException
     */

    public function data()
    {
        $appInstance = $this->messaging->getAppInstance("cnsLnniHQA-4P5eIODR8HP:APA91bFgA_TkTiyJQoqhYfVINC-nsAVq7k4wn39EBe3UBNeE771lWEn6cYpzcJQLc5dp8lMJSrSb43TXjIKOAA37dt-qRzGMDCOuePvSGFChZQ9ub1wI_fD6kNeBSiaRAFgpN6Fr2jJQ");
        // Return the full information as provided by the Firebase API
        return $appInstance->rawData();
    }
}
