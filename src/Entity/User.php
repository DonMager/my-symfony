<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private string $email;

    /**
     * @ORM\Column(type="json")
     */
    private array $roles = [];

    /**
     * @ORM\Column(type="string", name="password_hash")
     */
    private string $password;

    /**
     * @ORM\Column(type="string", length=180, name="last_name")
     */
    private string $lastName;

    /**
     * @ORM\Column(type="string", length=180, name="first_name")
     */
    private string $firstName;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private string $patronymic = '';

    /**
     * @ORM\Column(type="string", length=15)
     */
    private string $phone;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private int $sex=1;

    /**
     * @ORM\Column(type="string")
     */
    private string $verifyToken;

    /**
     * @ORM\Column(type="string")
     */
    private string $deviceToken = '';

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isPhoneEnabled = false;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="datetime_immutable")
     *
     */
    private \DateTimeImmutable $updatedAt;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('owner', 'relative', 'other')")
     */
    private string $registrationType = 'owner';

    /**
     * @ORM\Column(type="string")
     */
    private string $apiToken;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Realty", mappedBy="user")
     */
    private $realty;

    public function __construct()
    {
        $this->realty = new ArrayCollection();
    }

    /**
     * @return Collection|Realty[]
     */
    public function getRealty(): Collection
    {
        return $this->realty;
    }

    /**
     * @ORM\PrePersist
     */
    public function preSetCreatedAt(): void
    {
        $dateTimeImmutable = new \DateTimeImmutable();
        $this->setCreatedAt($dateTimeImmutable);
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function preSetUpdatedAt(): void
    {
        $dateTimeImmutable = new \DateTimeImmutable();
        $this->setUpdatedAt($dateTimeImmutable);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getApiToken(): string
    {
        return $this->apiToken;
    }

    public function setApiToken(string $apiToken): void
    {
        $this->apiToken = $apiToken;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function getPatronymic(): string
    {
        return $this->patronymic;
    }

    public function setPatronymic(string $patronymic): void
    {
        $this->patronymic = $patronymic;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    public function getSex(): int
    {
        return $this->sex;
    }

    public function setSex(int $sex): void
    {
        $this->sex = $sex;
    }

    public function getVerifyToken(): string
    {
        return $this->verifyToken;
    }

    public function setVerifyToken(string $verifyToken): void
    {
        $this->verifyToken = $verifyToken;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function isPhoneEnabled(): bool
    {
        return $this->isPhoneEnabled;
    }

    public function setIsPhoneEnabled(bool $isPhoneEnabled): void
    {
        $this->isPhoneEnabled = $isPhoneEnabled;
    }

    public function getRegistrationType(): string
    {
        return $this->registrationType;
    }

    public function setRegistrationType(string $registrationType): void
    {
        $this->registrationType = $registrationType;
    }

    public function getDeviceToken(): string
    {
        return $this->deviceToken;
    }

    public function setDeviceToken(string $deviceToken): void
    {
        $this->deviceToken = $deviceToken;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
