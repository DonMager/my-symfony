<?php

namespace App\Controller\Dictionary;

use App\Handler\Dictionary\House\AddHandler;
use App\Repository\Dictionary\HouseRepository;
use App\Serializer\Dictionary\HouseSerializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 *
 * @Route("/api/house", name="api_house")
 */
class HouseController extends AbstractController
{
    private const CODE_SUCCESS = 200;

    private HouseRepository $repository;
    private HouseSerializer $serializer;
    private AddHandler $addHandler;

    public function __construct(
        HouseRepository $repository,
        HouseSerializer $serializer,
        AddHandler $addHandler
    ) {
        $this->repository = $repository;
        $this->serializer = $serializer;
        $this->addHandler = $addHandler;
    }

    /**
     * @Route("/all", name="api_house_all", methods={"GET"})
     * @throws ExceptionInterface
     */
    public function getHouses(): JsonResponse
    {
        $query = $this->repository->findAll();

        return $this->response($this->serializer->normalize($query));
    }

    /**
     * @Route("/add", name="api_house_add", methods={"POST"})
     * @throws ExceptionInterface
     */
    public function add(Request $request): JsonResponse
    {
        try {
            $data = $this->getJsonDecodeData($request);
            $house = $this->serializer->denormalize($data);
            $this->repository->save($house);
            $data= ['dd'=>$house];
            return $this->response($data);
        } catch (\Exception $e) {
            $data =['error'=> $e->getMessage()];
            return $this->response($data, 450);
        }
    }

    /**
     * @Route("/{id}", name="api_house_show", methods={"GET"})
     * @throws ExceptionInterface
     * @throws \Exception
     */
    public function show(int $id): JsonResponse
    {
        $house = $this->repository->find($id);
        if ($house) {
            return $this->response($this->serializer->normalize($house));
        }
        throw new \Exception('Нет такой страницы');
    }

    /**
     * @Route("/update/{id}", name="api_house_update", methods={"PUT"})
     * @throws ExceptionInterface
     */
    public function update(int $id, Request $request): JsonResponse
    {
        try {
            $house = $this->repository->find($id);
            if ($house) {
                $data = $this->getJsonDecodeData($request);
                $house = $this->serializer->denormalize($data, $house);
                $this->repository->save($house);
                $data= ['dd'=>$house];
                return $this->response($data);
            }
            throw new \Exception('Нет такой страницы');
        } catch (\Exception $e) {
            //   $data = $this->getDataErrorMessage($e->getCode(), $e->getMessage());
            $data =['ddd'=> $e->getMessage()];
            return $this->response($data, 450);
        }
    }

    /**
     * @Route("/delete/{id}", name="api_house_delete", methods={"DELETE"})
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function delete(int $id): JsonResponse
    {
        $house = $this->repository->find($id);
        if ($house) {
            $this->repository->remove($house);
            return $this->response(['Удален']);
        }
        throw new \Exception('Нет такой страницы');
    }

    private function response(array $data, int $status = self::CODE_SUCCESS, array $headers = []): JsonResponse
    {
        return new JsonResponse($data, $status, $headers);
    }

    /**
     * @throws \Exception
     */
    private function getJsonDecodeData(Request $request): object
    {
        $body = $request->getContent();
        if (!is_string($body)) {
            throw new \Exception('Нет JSON-объекта');
        }
        $data = json_decode($body);
        if ($data) {
            return $data;
        }
        throw new \Exception('Нет данных');
    }
}
