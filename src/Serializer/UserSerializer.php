<?php

namespace App\Serializer;

use App\Entity\User;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class UserSerializer extends AbstractSerializer
{
    /**
     * @throws ExceptionInterface
     * @throws \Exception
     */
    public function denormalize(object $data, User $user = null): User
    {
        $context = $user ? [AbstractNormalizer::OBJECT_TO_POPULATE => $user] : [];
        $serializer = $this->getSerializer();
        $user = $serializer->denormalize($data, User::class, 'json', $context);
        if ($user instanceof User) {
            return $user;
        }
        throw new \Exception('Не является объектом User');
    }

    public function getContextIgnore()
    {
        return [AbstractNormalizer::IGNORED_ATTRIBUTES => ['user', 'realtyHouse']];
    }
}
