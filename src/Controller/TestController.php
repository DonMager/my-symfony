<?php

namespace App\Controller;

use App\Firebase\Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    private $message;

    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * @Route("/test", name="test")
     * @throws \Kreait\Firebase\Exception\FirebaseException
     * @throws \Kreait\Firebase\Exception\MessagingException
     */
    public function index(): Response
    {
        $this->message->send();
        return $this->json([
            'message' => $this->message->data(),
            'path' => 'src/Controller/TestController.php',
        ]);
    }
}
