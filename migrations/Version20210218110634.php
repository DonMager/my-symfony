<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210218110634 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE realty (user_id INT NOT NULL, house_id INT DEFAULT NULL, item_number DOUBLE PRECISION NOT NULL, square DOUBLE PRECISION NOT NULL, item_type ENUM(\'living\', \'commercial\', \'non-living\'), UNIQUE INDEX UNIQ_627221C6BB74515 (house_id), PRIMARY KEY(user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE realty ADD CONSTRAINT FK_627221CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE realty ADD CONSTRAINT FK_627221C6BB74515 FOREIGN KEY (house_id) REFERENCES house (id)');
        $this->addSql('ALTER TABLE user CHANGE registration_type registration_type ENUM(\'owner\', \'relative\', \'other\')');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE realty');
        $this->addSql('ALTER TABLE user CHANGE registration_type registration_type VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
